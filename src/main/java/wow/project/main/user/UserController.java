package wow.project.main.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(path="/user") //URL's start with /user (after Application path)
public class UserController {
	
	@Autowired // This means to get the bean called userRepository
	// Which is auto-generated by Spring, we will use it to handle the data
	private UserRepository userRepository;

	/*
	 * adding the data into the DB using the path: localhost:8080/user/add?name=john&email_address=john@john.au&password=John11&competency=12.34
	 */
	@GetMapping(path = "/add") // Map ONLY GET Requests
	public @ResponseBody String addNewUser(@RequestParam String name, @RequestParam String email_address, @RequestParam String password, @RequestParam Double competency) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request

		User n = new User();
		n.setName(name);
		n.setEmail_address(email_address);
		n.setPassword(password);
		n.setCompetency(competency);
		userRepository.save(n);
		return "Saved";
	}
	
	//deleting the row in DB after the path: localhost:8080/user/delete?user_id=0
	@GetMapping(path = "/delete") // Map ONLY GET Requests
	public @ResponseBody String deleteUser(@RequestParam Integer user_id) {

		userRepository.delete(user_id);
		return "Deleted";
	}

	//displaying all rows from DB localhost:8080/user/all
	@GetMapping(path = "/all")
	public @ResponseBody Iterable<User> getAllUsers() {
		// This returns a JSON or XML with the users
		return userRepository.findAll();
	}
	
	//finding and displaying only one row from the DB: localhost:8080/user/getone?user_id=1
	@GetMapping(path = "/getone")
	public @ResponseBody User getUser(@RequestParam Integer user_id) {
		
		return userRepository.findOne(user_id);
	}
	
	/*
	 *updating the row in DB after the path call: localhost:8080/user/update?user_id=1&name=Shaun&email_address=shaun@john.au&password=Shaun&competency=123.23
	 */
	@GetMapping(path = "/update")
	public @ResponseBody User updateUser(@RequestParam Integer user_id, User user) {
		
		return userRepository.save(user);
	}	
}
