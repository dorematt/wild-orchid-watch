package wow.project.main.media;

import org.springframework.data.repository.CrudRepository;

public interface MediaRepository extends CrudRepository<Media, Integer> {

}
