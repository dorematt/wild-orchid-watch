package wow.project.main.media;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Media {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer med_id;

	private String file;
	
	private Integer sighting;
	
	private String date; 
    
    private Integer user;
    
    private Boolean identifying_flag;

	/**
	 * @return the file
	 */
	public String getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(String file) {
		this.file = file;
	}

	/**
	 * @return the sighting
	 */
	public Integer getSighting() {
		return sighting;
	}

	/**
	 * @param sighting the sighting to set
	 */
	public void setSighting(Integer sighting) {
		this.sighting = sighting;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the user
	 */
	public Integer getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(Integer user) {
		this.user = user;
	}

	/**
	 * @return the identifying_flag
	 */
	public Boolean getIdentifying_flag() {
		return identifying_flag;
	}

	/**
	 * @param identifying_flag the identifying_flag to set
	 */
	public void setIdentifying_flag(Boolean identifying_flag) {
		this.identifying_flag = identifying_flag;
	}

}
