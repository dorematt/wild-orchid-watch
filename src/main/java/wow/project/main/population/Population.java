package wow.project.main.population;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Population {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer pop_id;

	private String latitude, longitude, description;
    
    private Integer species, parent_population;
 
	/**
	 * @return the pop_id
	 */
	public Integer getPop_id() {
		return pop_id;
	}

	/**
	 * @param pop_id the pop_id to set
	 */
	public void setPop_id(Integer pop_id) {
		this.pop_id = pop_id;
	}

	/**
	 * @return the altitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the species
	 */
	public Integer getSpecies() {
		return species;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(Integer species) {
		this.species = species;
	}

	/**
	 * @return the parent_population
	 */
	public Integer getParent_population() {
		return parent_population;
	}

	/**
	 * @param parent_population the parent_population to set
	 */
	public void setParent_population(Integer parent_population) {
		this.parent_population = parent_population;
	}
}
