package wow.project.main.population;

import org.springframework.data.repository.CrudRepository;

public interface PopulationRepository extends CrudRepository<Population, Integer> {

}
