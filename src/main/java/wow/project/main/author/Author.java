package wow.project.main.author;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Author {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer auth_id;

	private String name;

	/**
	 * @return the auth_id
	 */
	public Integer getAuth_id() {
		return auth_id;
	}

	/**
	 * @param auth_id the auth_id to set
	 */
	public void setAuth_id(Integer auth_id) {
		this.auth_id = auth_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
