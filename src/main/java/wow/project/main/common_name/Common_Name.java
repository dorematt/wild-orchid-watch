package wow.project.main.common_name;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Common_Name {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer cname_id;

	private String date;
	
	private Integer species;
	
	/**
	 * @return the cname_id
	 */
	public Integer getCname_id() {
		return cname_id;
	}

	/**
	 * @param cname_id the cname_id to set
	 */
	public void setCname_id(Integer cname_id) {
		this.cname_id = cname_id;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * @return the species
	 */
	public Integer getSpecies() {
		return species;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(Integer species) {
		this.species = species;
	}
}
