package wow.project.main.common_name;

import org.springframework.data.repository.CrudRepository;

public interface Common_NameRepository extends CrudRepository<Common_Name, Integer> {

}
