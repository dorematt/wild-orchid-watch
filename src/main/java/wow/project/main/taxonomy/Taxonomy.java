package wow.project.main.taxonomy;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Taxonomy {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer tax_id;

	private String name;
	
	private Integer species;
	
	private Integer genus;
	
	private Integer publication;

	/**
	 * @return the tax_id
	 */
	public Integer getTax_id() {
		return tax_id;
	}

	/**
	 * @param tax_id the tax_id to set
	 */
	public void setTax_id(Integer tax_id) {
		this.tax_id = tax_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the species
	 */
	public Integer getSpecies() {
		return species;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(Integer species) {
		this.species = species;
	}

	/**
	 * @return the genus
	 */
	public Integer getGenus() {
		return genus;
	}

	/**
	 * @param genus the genus to set
	 */
	public void setGenus(Integer genus) {
		this.genus = genus;
	}

	/**
	 * @return the publication
	 */
	public Integer getPublication() {
		return publication;
	}

	/**
	 * @param publication the publication to set
	 */
	public void setPublication(Integer publication) {
		this.publication = publication;
	}

}
