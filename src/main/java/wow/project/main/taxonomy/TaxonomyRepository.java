package wow.project.main.taxonomy;

import org.springframework.data.repository.CrudRepository;

public interface TaxonomyRepository extends CrudRepository<Taxonomy, Integer> {

}
