package wow.project.main.species;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Species {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer species_id;

	private String description, recognition, habitat, habit, plant_size, general_lifeform, leaves, flower_stem, 
	flower, petals, galea, sepals, labellum, collumn, fragrance, roots, tuber, reproduction, pseudobulb, 
	inflorescence, peduncle, scape, sympodial_monopodial_growth, distribution_notes, conservation_status;
	
	private Integer flowering_start, flowering_end;
	
	private String pollination_notes, propogation_notes, cultivation_notes, further_notes;
	
	private Boolean hybrid_flag;
	
	private String created_date, modified_date;
	
    private Integer modified_by, created_by, species_parent;

    /**
	 * @return the species_id
	 */
    public Integer getSight_id() {
		return species_id;
	}

    /**
	 * @param species_id the species_id to set
	 */
	public void setSight_id(Integer species_id) {
		this.species_id = species_id;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the recognition
	 */
	public String getRecognition() {
		return recognition;
	}

	/**
	 * @param recognition the recognition to set
	 */
	public void setRecognition(String recognition) {
		this.recognition = recognition;
	}

	/**
	 * @return the habitat
	 */
	public String getHabitat() {
		return habitat;
	}

	/**
	 * @param habitat the habitat to set
	 */
	public void setHabitat(String habitat) {
		this.habitat = habitat;
	}

	/**
	 * @return the habit
	 */
	public String getHabit() {
		return habit;
	}

	/**
	 * @param habit the habit to set
	 */
	public void setHabit(String habit) {
		this.habit = habit;
	}

	/**
	 * @return the plant_size
	 */
	public String getPlant_size() {
		return plant_size;
	}

	/**
	 * @param plant_size the plant_size to set
	 */
	public void setPlant_size(String plant_size) {
		this.plant_size = plant_size;
	}

	/**
	 * @return the general_lifeform
	 */
	public String getGeneral_lifeform() {
		return general_lifeform;
	}

	/**
	 * @param general_lifeform the general_lifeform to set
	 */
	public void setGeneral_lifeform(String general_lifeform) {
		this.general_lifeform = general_lifeform;
	}

	/**
	 * @return the leaves
	 */
	public String getLeaves() {
		return leaves;
	}

	/**
	 * @param leaves the leaves to set
	 */
	public void setLeaves(String leaves) {
		this.leaves = leaves;
	}

	/**
	 * @return the flower_stem
	 */
	public String getFlower_stem() {
		return flower_stem;
	}

	/**
	 * @param flower_stem the flower_stem to set
	 */
	public void setFlower_stem(String flower_stem) {
		this.flower_stem = flower_stem;
	}

	/**
	 * @return the flower
	 */
	public String getFlower() {
		return flower;
	}

	/**
	 * @param flower the flower to set
	 */
	public void setFlower(String flower) {
		this.flower = flower;
	}

	/**
	 * @return the petals
	 */
	public String getPetals() {
		return petals;
	}

	/**
	 * @param petals the petals to set
	 */
	public void setPetals(String petals) {
		this.petals = petals;
	}

	/**
	 * @return the galea
	 */
	public String getGalea() {
		return galea;
	}

	/**
	 * @param galea the galea to set
	 */
	public void setGalea(String galea) {
		this.galea = galea;
	}

	/**
	 * @return the sepals
	 */
	public String getSepals() {
		return sepals;
	}

	/**
	 * @param sepals the sepals to set
	 */
	public void setSepals(String sepals) {
		this.sepals = sepals;
	}

	/**
	 * @return the labellum
	 */
	public String getLabellum() {
		return labellum;
	}

	/**
	 * @param labellum the labellum to set
	 */
	public void setLabellum(String labellum) {
		this.labellum = labellum;
	}

	/**
	 * @return the collumn
	 */
	public String getCollumn() {
		return collumn;
	}

	/**
	 * @param column the column to set
	 */
	public void setCollumn(String collumn) {
		this.collumn = collumn;
	}

	/**
	 * @return the fragrance
	 */
	public String getFragrance() {
		return fragrance;
	}

	/**
	 * @param fragrance the fragrance to set
	 */
	public void setFragrance(String fragrance) {
		this.fragrance = fragrance;
	}

	/**
	 * @return the roots
	 */
	public String getRoots() {
		return roots;
	}

	/**
	 * @param roots the roots to set
	 */
	public void setRoots(String roots) {
		this.roots = roots;
	}

	/**
	 * @return the tuber
	 */
	public String getTuber() {
		return tuber;
	}

	/**
	 * @param tuber the tuber to set
	 */
	public void setTuber(String tuber) {
		this.tuber = tuber;
	}

	/**
	 * @return the reproduction
	 */
	public String getReproduction() {
		return reproduction;
	}

	/**
	 * @param reproduction the reproduction to set
	 */
	public void setReproduction(String reproduction) {
		this.reproduction = reproduction;
	}

	/**
	 * @return the pseudobulb
	 */
	public String getPseudobulb() {
		return pseudobulb;
	}

	/**
	 * @param pseudobulb the pseudobulb to set
	 */
	public void setPseudobulb(String pseudobulb) {
		this.pseudobulb = pseudobulb;
	}

	/**
	 * @return the inflorescence
	 */
	public String getInflorescence() {
		return inflorescence;
	}

	/**
	 * @param inflorescence the inflorescence to set
	 */
	public void setInflorescence(String inflorescence) {
		this.inflorescence = inflorescence;
	}

	/**
	 * @return the peduncle
	 */
	public String getPeduncle() {
		return peduncle;
	}

	/**
	 * @param peduncle the peduncle to set
	 */
	public void setPeduncle(String peduncle) {
		this.peduncle = peduncle;
	}

	/**
	 * @return the scape
	 */
	public String getScape() {
		return scape;
	}

	/**
	 * @param scape the scape to set
	 */
	public void setScape(String scape) {
		this.scape = scape;
	}

	/**
	 * @return the sympodial_monopodial_growth
	 */
	public String getSympodial_monopodial_growth() {
		return sympodial_monopodial_growth;
	}

	/**
	 * @param sympodial_monopodial_growth the sympodial_monopodial_growth to set
	 */
	public void setSympodial_monopodial_growth(String sympodial_monopodial_growth) {
		this.sympodial_monopodial_growth = sympodial_monopodial_growth;
	}

	/**
	 * @return the distribution_notes
	 */
	public String getDistribution_notes() {
		return distribution_notes;
	}

	/**
	 * @param distribution_notes the distribution_notes to set
	 */
	public void setDistribution_notes(String distribution_notes) {
		this.distribution_notes = distribution_notes;
	}

	/**
	 * @return the conservation_status
	 */
	public String getConservation_status() {
		return conservation_status;
	}

	/**
	 * @param conservation_status the conservation_status to set
	 */
	public void setConservation_status(String conservation_status) {
		this.conservation_status = conservation_status;
	}

	/**
	 * @return the flowering_start
	 */
	public Integer getFlowering_start() {
		return flowering_start;
	}

	/**
	 * @param flowering_start the flowering_start to set
	 */
	public void setFlowering_start(Integer flowering_start) {
		this.flowering_start = flowering_start;
	}

	/**
	 * @return the flowering_end
	 */
	public Integer getFlowering_end() {
		return flowering_end;
	}

	/**
	 * @param flowering_end the flowering_end to set
	 */
	public void setFlowering_end(Integer flowering_end) {
		this.flowering_end = flowering_end;
	}

	/**
	 * @return the pollination_notes
	 */
	public String getPollination_notes() {
		return pollination_notes;
	}

	/**
	 * @param pollination_notes the pollination_notes to set
	 */
	public void setPollination_notes(String pollination_notes) {
		this.pollination_notes = pollination_notes;
	}

	/**
	 * @return the propogation_notes
	 */
	public String getPropogation_notes() {
		return propogation_notes;
	}

	/**
	 * @param propogation_notes the propogation_notes to set
	 */
	public void setPropogation_notes(String propogation_notes) {
		this.propogation_notes = propogation_notes;
	}

	/**
	 * @return the cultivation_notes
	 */
	public String getCultivation_notes() {
		return cultivation_notes;
	}

	/**
	 * @param cultivation_notes the cultivation_notes to set
	 */
	public void setCultivation_notes(String cultivation_notes) {
		this.cultivation_notes = cultivation_notes;
	}

	/**
	 * @return the further_notes
	 */
	public String getFurther_notes() {
		return further_notes;
	}

	/**
	 * @param further_notes the further_notes to set
	 */
	public void setFurther_notes(String further_notes) {
		this.further_notes = further_notes;
	}

	/**
	 * @return the hybrid_flag
	 */
	public Boolean getHybrid_flag() {
		return hybrid_flag;
	}

	/**
	 * @param hybrid_flag the hybrid_flag to set
	 */
	public void setHybrid_flag(Boolean hybrid_flag) {
		this.hybrid_flag = hybrid_flag;
	}

	/**
	 * @return the created_date
	 */
	public String getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date the created_date to set
	 */
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the modified_date
	 */
	public String getModified_date() {
		return modified_date;
	}

	/**
	 * @param modified_date the modified_date to set
	 */
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}

	/**
	 * @return the modified_by
	 */
	public Integer getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by the modified_by to set
	 */
	public void setModified_by(Integer modified_by) {
		this.modified_by = modified_by;
	}

	/**
	 * @return the created_by
	 */
	public Integer getCreated_by() {
		return created_by;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	/**
	 * @return the species_parent
	 */
	public Integer getSpecies_parent() {
		return species_parent;
	}

	/**
	 * @param species_parent the species_parent to set
	 */
	public void setSpecies_parent(Integer species_parent) {
		this.species_parent = species_parent;
	}
    
}
