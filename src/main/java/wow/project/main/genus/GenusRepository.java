package wow.project.main.genus;

import org.springframework.data.repository.CrudRepository;

public interface GenusRepository extends CrudRepository<Genus, Integer> {

}
