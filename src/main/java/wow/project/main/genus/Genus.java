package wow.project.main.genus;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Genus {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer genus_id;

	private String etymology;
	
	private String name;
	
	private String family;
	
	private String description;
	
	private String pollination;
	
	private String created_date;
	
	private String modified_date;
	
	private Integer	created_by;
	
	private Integer modified_by;

	/**
	 * @return the genus_id
	 */
	public Integer getGenus_id() {
		return genus_id;
	}

	/**
	 * @param genus_id the genus_id to set
	 */
	public void setGenus_id(Integer genus_id) {
		this.genus_id = genus_id;
	}

	/**
	 * @return the etymology
	 */
	public String getEtymology() {
		return etymology;
	}

	/**
	 * @param etymology the etymology to set
	 */
	public void setEtymology(String etymology) {
		this.etymology = etymology;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the family
	 */
	public String getFamily() {
		return family;
	}

	/**
	 * @param family the family to set
	 */
	public void setFamily(String family) {
		this.family = family;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the pollination
	 */
	public String getPollination() {
		return pollination;
	}

	/**
	 * @param pollination the pollination to set
	 */
	public void setPollination(String pollination) {
		this.pollination = pollination;
	}

	/**
	 * @return the created_date
	 */
	public String getCreated_date() {
		return created_date;
	}

	/**
	 * @param created_date the created_date to set
	 */
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	/**
	 * @return the modified_date
	 */
	public String getModified_date() {
		return modified_date;
	}

	/**
	 * @param modified_date the modified_date to set
	 */
	public void setModified_date(String modified_date) {
		this.modified_date = modified_date;
	}

	/**
	 * @return the created_by
	 */
	public Integer getCreated_by() {
		return created_by;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(Integer created_by) {
		this.created_by = created_by;
	}

	/**
	 * @return the modified_by
	 */
	public Integer getModified_by() {
		return modified_by;
	}

	/**
	 * @param modified_by the modified_by to set
	 */
	public void setModified_by(Integer modified_by) {
		this.modified_by = modified_by;
	}
	
	
}
