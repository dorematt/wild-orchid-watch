package wow.project.main.user_group;

import org.springframework.data.repository.CrudRepository;

public interface User_GroupRepository extends CrudRepository<User_Group, Integer> {

}
