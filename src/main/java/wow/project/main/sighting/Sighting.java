package wow.project.main.sighting;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity // This tells Hibernate to make a table out of this class
public class Sighting {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer sight_id;

	private Integer population;
	
	private String date;

    private Integer quantity;
    
    private Double confidence;
    
    private String count_type, health;
        
    private Integer leaves_only_percent, leaves_stems_percent, flowering_percent, 
    seeded_percent, fruiting_percent, desiccated_percent;
    
    private Double latitude_update, longitude_update;
    
    private Integer sighting_by;
    
    /**
	 * @return the sight_id
	 */
    public Integer getSight_id() {
		return sight_id;
	}

    /**
	 * @param sight_id the sight_id to set
	 */
	public void setSight_id(Integer sight_id) {
		this.sight_id = sight_id;
	}

	/**
	 * @return the population
	 */
	public Integer getPopulation() {
		return population;
	}

	/**
	 * @param population the population to set
	 */
	public void setPopulation(Integer population) {
		this.population = population;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * @return the confidence
	 */
	public Double getConfidence() {
		return confidence;
	}

	/**
	 * @param confidence the confidence to set
	 */
	public void setConfidence(Double confidence) {
		this.confidence = confidence;
	}

	/**
	 * @return the count_type
	 */
	public String getCount_type() {
		return count_type;
	}

	/**
	 * @param count_type the count_type to set
	 */
	public void setCount_type(String count_type) {
		this.count_type = count_type;
	}

	/**
	 * @return the health
	 */
	public String getHealth() {
		return health;
	}

	/**
	 * @param health the health to set
	 */
	public void setHealth(String health) {
		this.health = health;
	}

	/**
	 * @return the leaves_only_percent
	 */
	public Integer getLeaves_only_percent() {
		return leaves_only_percent;
	}

	/**
	 * @param leaves_only_percent the leaves_only_percent to set
	 */
	public void setLeaves_only_percent(Integer leaves_only_percent) {
		this.leaves_only_percent = leaves_only_percent;
	}

	/**
	 * @return the leaves_stems_percent
	 */
	public Integer getLeaves_stems_percent() {
		return leaves_stems_percent;
	}

	/**
	 * @param leaves_stems_percent the leaves_stems_percent to set
	 */
	public void setLeaves_stems_percent(Integer leaves_stems_percent) {
		this.leaves_stems_percent = leaves_stems_percent;
	}

	/**
	 * @return the flowering_percent
	 */
	public Integer getFlowering_percent() {
		return flowering_percent;
	}

	/**
	 * @param flowering_percent the flowering_percent to set
	 */
	public void setFlowering_percent(Integer flowering_percent) {
		this.flowering_percent = flowering_percent;
	}

	/**
	 * @return the seeded_percent
	 */
	public Integer getSeeded_percent() {
		return seeded_percent;
	}

	/**
	 * @param seeded_percent the seeded_percent to set
	 */
	public void setSeeded_percent(Integer seeded_percent) {
		this.seeded_percent = seeded_percent;
	}

	/**
	 * @return the fruiting_percent
	 */
	public Integer getFruiting_percent() {
		return fruiting_percent;
	}

	/**
	 * @param fruiting_percent the fruiting_percent to set
	 */
	public void setFruiting_percent(Integer fruiting_percent) {
		this.fruiting_percent = fruiting_percent;
	}

	/**
	 * @return the desiccated_percent
	 */
	public Integer getDesiccated_percent() {
		return desiccated_percent;
	}

	/**
	 * @param desiccated_percent the desiccated_percent to set
	 */
	public void setDesiccated_percent(Integer desiccated_percent) {
		this.desiccated_percent = desiccated_percent;
	}

	/**
	 * @return the latitude_update
	 */
	public Double getLatitude_update() {
		return latitude_update;
	}

	/**
	 * @param latitude_update the latitude_update to set
	 */
	public void setLatitude_update(Double latitude_update) {
		this.latitude_update = latitude_update;
	}

	/**
	 * @return the longitude_update
	 */
	public Double getLongitude_update() {
		return longitude_update;
	}

	/**
	 * @param longitude_update the longitude_update to set
	 */
	public void setLongitude_update(Double longitude_update) {
		this.longitude_update = longitude_update;
	}

	/**
	 * @return the sighting_by
	 */
	public Integer getSighting_by() {
		return sighting_by;
	}

	/**
	 * @param sighting_by the sighting_by to set
	 */
	public void setSighting_by(Integer sighting_by) {
		this.sighting_by = sighting_by;
	}
}
