package wow.project.main.sighting;

import org.springframework.data.repository.CrudRepository;

public interface SightingRepository extends CrudRepository<Sighting, Integer> {

}
