package wow.project.main.publication;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity // This tells Hibernate to make a table out of this class
public class Publication {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer pub_id;

	private String name;
	
	private String date;

	/**
	 * @return the pub_id
	 */
	public Integer getPub_id() {
		return pub_id;
	}

	/**
	 * @param pub_id the pub_id to set
	 */
	public void setPub_id(Integer pub_id) {
		this.pub_id = pub_id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the date
	 */
	public String getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

}
